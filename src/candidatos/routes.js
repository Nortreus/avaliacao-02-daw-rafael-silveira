module.exports = (app) => {

    const controller = require('./controller')


    app.post('/candidatos', controller.create)


    app.get('/candidatos', controller.findAll)

    app.delete('/candidatos', controller.remover)

    app.put('/candidatos', controller.update)

}
