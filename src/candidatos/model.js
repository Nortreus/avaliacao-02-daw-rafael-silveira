const db = require("./../configs/sequelize")
const { Model, DataTypes } = db.Sequelize

const sequelize = db.sequelize

class Candidato extends Model { }
Candidato.init({
    cpf: {
        type: DataTypes.STRING,

    },
    nome: {
        type: DataTypes.STRING
    },
    datanascimento: {
        type: DataTypes.DATE
    },
    telefone: {
        type: DataTypes.STRING
    },
    profissao: {
        type: DataTypes.STRING
    },
    escolaridade: {
        type: DataTypes.STRING
    },
    habilidades: {
        type: DataTypes.STRING
    }

}, { sequelize, modelName: "candidato" })

module.exports = Candidato
