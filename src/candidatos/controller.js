const db = require("./../configs/sequelize")
const Candidato = require("./model")

const { Op } = db.Sequelize

exports.create = (req, res) => {
    Candidato.findOrCreate({
        where: {
            cpf: req.body.cpf,
        },
        defaults: {
            cpf: req.body.cpf,
            nome: req.body.nome,
            datanascimento: req.body.datanascimento,
            telefone: req.body.telefone,
            profissao: req.body.profissao,
            escolaridade: req.body.escolaridade,
            habilidades: req.body.habilidades
        }

    }).then((candidato) => {
        res.send(candidato)
    })
}

exports.findAll = (req, res) => {
    Candidato.findAll({
        where: {
            [Op.or]: [{ nome: { [Op.like]: req.query.nome } },
            { cpf: { [Op.like]: req.query.cpf } },
            { datanascimento: { [Op.between]: [req.query.dataIni, req.query.dataFim] } }]
        }
    }).then((candidatos) => {
        res.send(candidatos)
    })
}

exports.remover = (req, res) => {
    Candidato.destroy({
        where: {
            id: req.body.id
        }
    }).then((affectedRows) => {
        res.send({ 'message': 'ok', 'affectedRows': affectedRows })
    })
}

exports.update = (req, res) => {
    Candidato.update({
        cpf: req.body.cpf,
        nome: req.body.nome,
        datanascimento: req.body.datanascimento,
        telefone: req.body.telefone,
        profissao: req.body.profissao,
        escolaridade: req.body.escolaridade,
        habilidades: req.body.habilidades
    },
        {
            where: {
                id: req.body.id
            }
        }).then((candidato) => {
            res.send(candidato)
        })
}