var nBusca;
Candidato = {
    cadastrar: () => {
        var cad = {};

        cad.cpf = $("#cadCpf");
        cad.nome = $("#cadNome");
        cad.datanascimento = $("#cadDtNascimento");
        cad.telefone = $("#cadTelefone");
        cad.profissao = $("#cadProff");
        cad.escolaridade = $("#cadEscolaridade");
        cad.habilidades = $("#cadHabilidades");

        var cont = 0;
        if (cad.nome.val().length > 40 || cad.nome.val() == "") {
            cad.nome.addClass("invalido");
            cont += 1;
            $("#verCadNome").html("Por favor informe um nome valido!!!");

        }
        else {
            cad.nome.removeClass("invalido");
            cad.nome.addClass("valido");
            $("#verCadNome").html("Digite o nome");
        }

        if (Candidato.validarCpf(cad.cpf) == false || cad.cpf.val() == "") {
            cad.cpf.addClass("invalido");
            cont += 1;
            $("#verCadCpf").html("Por favor informe um cpf valido!!!");
        } else {
            cad.cpf.removeClass("invalido");
            cad.cpf.addClass("valido");
            $("#verCadCpf").html("Digite o CPF");
        }

        if (cad.datanascimento.val() == "") {
            cad.datanascimento.addClass("invalido");
            cont += 1;
            $("#vercadDtNascimento").html("Por favor informe uma data de nascimento valida!!!");
        } else {
            cad.datanascimento.removeClass("invalido");
            cad.datanascimento.addClass("valido");
            $("#vercadDtNascimento").html("Digite a data de nascimento");
        }

        if (cad.telefone.val() == "") {
            cad.telefone.addClass("invalido");
            cont += 1;
            $("#vercadTelefone").html("Por favor informe o telefone!!!");

        } else {
            cad.telefone.removeClass("invalido");
            cad.telefone.addClass("valido");
            $("#vercadTelefone").html("Digite o telefone");

        }
        if (cad.profissao.val() == "") {
            cad.profissao.addClass("invalido");
            cont += 1;
            $("#vercadProff").html("Por favor informe a profissão!!!");
        } else {
            cad.profissao.removeClass("invalido");
            cad.profissao.addClass("valido");
            $("#vercadProff").html("Digite a profissão");
        }
        if (cad.escolaridade.val() == "") {
            cad.escolaridade.addClass("invalido");
            cont += 1;
            $("#vercadEscolaridade").html("Por favor informe a escolaridade!!!");
        } else {
            cad.escolaridade.removeClass("invalido");
            cad.escolaridade.addClass("valido");
            $("#vercadEscolaridade").html("Digite a escolaridade");
        }
        if (cad.habilidades.val() == "") {
            cad.habilidades.addClass("invalido");
            cont += 1;
            $("#vercadHabilidades").html("Por favor informe as habilidades!!!");
        } else {
            cad.habilidades.removeClass("invalido");
            cad.habilidades.addClass("valido");
            $("#vercadHabilidades").html("Digite as habilidades");
        }

        if (cont == 0) {
            cad.cpf = cad.cpf.val();
            cad.nome = cad.nome.val();
            cad.datanascimento = cad.datanascimento.val();
            cad.telefone = cad.telefone.val();
            cad.profissao = cad.profissao.val();
            cad.escolaridade = cad.escolaridade.val();
            cad.habilidades = cad.habilidades.val();

            $.ajax({
                type: "POST",
                url: "/candidatos",
                data: cad,
                success: (data) => {
                    if (data[1] == true) {
                        console.log("Cadastrado com sucesso!!!");

                    } else {
                        console.log("Ja cadastrado!!!");
                        $("#cadCpf").removeClass("valido");
                        $("#cadCpf").addClass("invalido");
                        $("#verCadCpf").html("CPF ja cadastrado!!!");

                    }

                },
                error: () => {
                    console.log("Ocorreu um erro!!!");
                },
                dataType: "json"
            })
        }


    },
    validarCpf: (x) => {



        var cpf = x.val()

        if (cpf.length == 11) {

            var v = [];

            //Calcula o primeiro dígito de verificação.
            v[0] = 1 * cpf[0] + 2 * cpf[1] + 3 * cpf[2];
            v[0] += 4 * cpf[3] + 5 * cpf[4] + 6 * cpf[5];
            v[0] += 7 * cpf[6] + 8 * cpf[7] + 9 * cpf[8];
            v[0] = v[0] % 11;
            v[0] = v[0] % 10;

            //Calcula o segundo dígito de verificação.
            v[1] = 1 * cpf[1] + 2 * cpf[2] + 3 * cpf[3];
            v[1] += 4 * cpf[4] + 5 * cpf[5] + 6 * cpf[6];
            v[1] += 7 * cpf[7] + 8 * cpf[8] + 9 * v[0];
            v[1] = v[1] % 11;
            v[1] = v[1] % 10;

            //Retorna Verdadeiro se os dígitos de verificação são os esperados.

            if ((v[0] != cpf[9]) || (v[1] != cpf[10])) { return false }

            else if (cpf[0] == cpf[1] && cpf[1] == cpf[2] && cpf[2] == cpf[3] && cpf[3] == cpf[4] && cpf[4] == cpf[5] && cpf[5] == cpf[6] && cpf[6] == cpf[7] && cpf[7] == cpf[8] && cpf[8] == cpf[9] && cpf[9] == cpf[10]) { return false }

            else { return true }


        } else { return false } // 11


    }
    ,
    template: (data) => {
        var cand = $("<div></div>").attr("id", data.id).attr("class", "row");

        var nomeCandidato = $("<div></div>").attr("class", "cand col-5").html(data.nome);
        var cpfCandidato = $("<div></div>").attr("class", "cand col-5").html(data.cpf);
        var btnCandidato = $("<div></div>").attr("class", "cand col-2");

        var btnRemove = $('<button></button>').attr('class', 'btn-grad2').html('Remover');
        $(btnRemove).on('click', (event) => {
            Candidato.remover(event.target);
        })

        $(btnCandidato).append(btnRemove);

        $(cand).append(nomeCandidato);
        $(cand).append(cpfCandidato);
        $(cand).append(btnCandidato);

        $("#espacoCand").append(cand);











    },
    criarCabecalho: () => {
        $("#espacoCand").empty();
        $("#buscaNome").val("");
        $("#buscaDataIni").val("");
        $("#buscaDataFim").val("");
        $("#buscaCpf").val("");
        var cand = $("<div></div>").attr("class", "cand row");

        var nomeCandidato = $("<div></div>").attr("class", "col-5").html("Nome");
        var cpfCandidato = $("<div></div>").attr("class", "col-7").html("CPF");


        $(cand).append(nomeCandidato);
        $(cand).append(cpfCandidato);

        $("#espacoCand").append(cand);
    },
    findAll: () => {
        var bus = {};

        if ($("#buscaNome").val() != "" || $("#buscaCpf").val() != "" ||
            $("#buscaDataIni").val() != "" ||
            $("#buscaDataFim").val() != "") {
            if ($("#buscaNome").val() != "") {
                bus.nome = "%" + $("#buscaNome").val() + "%";
            }
            if ($("#buscaCpf").val() != "") {
                bus.cpf = "%" + $("#buscaCpf").val() + "%";
            }

            bus.dataIni = $("#buscaDataIni").val();
            bus.dataFim = $("#buscaDataFim").val();

            $.ajax({
                type: "GET",
                url: "/candidatos",
                data: bus,
                success: (data) => {
                    Candidato.criarCabecalho();
                    for (var cand of data) {
                        Candidato.template(cand);
                    }
                },
                error: () => {
                    console.log("Ocorreu um erro!!!");
                },
                dataType: "json"
            })
        } else {
            bus.nome = "%%";
            bus.cpf = "%%";
            bus.dataIni = "1000-01-01";
            bus.dataFim = "9999-12-31";
            Candidato.criarCabecalho();
            $.ajax({
                type: "GET",
                url: "/candidatos",
                data: bus,
                success: (data) => {
                    for (var cand of data) {
                        Candidato.template(cand);
                    }
                },
                error: () => {
                    console.log("Ocorreu um erro!!!");
                },
                dataType: "json"
            })
        }

    },
    remover: (button) => {
        var comment = $(button).parent().parent();

        var id = $(comment).attr('id').replace('comment-', '');


        $.ajax({
            type: "DELETE",
            url: "/candidatos",
            data: { 'id': id },
            success: (data) => {
                $(comment).remove();
            },
            error: () => {
                console.log("Ocorreu um erro");
            },
            dataType: 'json'
        })
    },
    update: () => {
        var up = {};
        up.id = nBusca;
        up.nome = $("#edNome").val();
        up.cpf = $("#edCpf").val();
        up.datanascimento = $("#edDtNascimento").val();
        up.telefone = $("#edTelefone").val();
        up.profissao = $("#edProff").val();
        up.escolaridade = $("#edEscolaridade").val();
        up.habilidades = $("#edHabilidades").val();

        if (Candidato.validarCpf($("#edCpf")) == false) {
            $("#edCpf").addClass("invalido");
        } else {
            $("#edCpf").removeClass("invalido");
            $("#edCpf").addClass("valido");
            $.ajax({
                type: "PUT",
                url: "/candidatos",
                data: up,
                success: (data) => {

                },
                error: () => {
                    console.log("Ocorreu um erro");
                },
                dataType: 'json'
            })
        }
    },

    buscar: () => {
        cpf = {};
        cpf.cpf = $("#edBuscaCpf");

        if (cpf.cpf.val() == "" || Candidato.validarCpf(cpf.cpf) == false) {
            cpf.cpf.addClass("invalido");
        } else {
            cpf.cpf.removeClass("invalido");
            cpf.cpf.addClass("valido");
            cpf.cpf = cpf.cpf.val();
            $.ajax({
                type: "GET",
                url: "/candidatos",
                data: cpf,
                success: (data) => {
                    console.log(data);
                    if (data.length != 0) {
                        Candidato.constroiEd(data);
                    } else {
                        alert("Candidato não encontrado!!!");
                    }

                },
                error: () => {
                    console.log("Ocorreu um erro!!!");
                },
                dataType: "json"
            })
        }



    },
    constroiEd: (data) => {
        $("#edNome").val(data[0].nome);
        $("#edCpf").val(data[0].cpf);
        $("#edDtNascimento").val(data[0].datanascimento);
        $("#edTelefone").val(data[0].telefone);
        $("#edProff").val(data[0].profissao);
        $("#edEscolaridade").val(data[0].escolaridade);
        $("#edHabilidades").val(data[0].habilidades);
        nBusca = data[0].id;
    }


}
$(document).ready(() => {
    Candidato.findAll();
})










