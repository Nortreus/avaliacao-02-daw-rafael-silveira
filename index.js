const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const db = require('./src/configs/sequelize')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.use(express.static('public'))

db.sequelize.authenticate().then(() => {
    console.log("Banco iniciado")
}).catch(err => {
    console.log(err + " Não foi possivel conectar!!!")
})


require('./src/candidatos/routes')(app)

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/public/view/index.html")
})

app.get("/cadastro", (req, res) => {
    res.sendFile(__dirname + "/public/view/cad.html")
})

app.get("/edicao", (req, res) => {
    res.sendFile(__dirname + "/public/view/ed.html")
})

var server = app.listen(3000, () => {
    console.log("Servidor rodando na porta " + server.address.port + " no host " + server.address().address)
})
